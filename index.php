<?php
require_once __DIR__ . '/vendor/autoload.php';

const RESOURCE_DIR = __DIR__ . '/data';

$resourceLoader = new \Lpp\Common\ResourceLoader(RESOURCE_DIR);
$collectionBuilder = new \Lpp\Builder\CollectionBuilder();
$itemService = new \Lpp\Service\Item\ItemService($resourceLoader, $collectionBuilder);
$unorderedBrandService = new \Lpp\Service\Brand\UnorderedBrandService($itemService);
$orderedByItemBrandService = new \Lpp\Service\Brand\OrderedByItemNameBrandService($itemService);

try {
    foreach ($unorderedBrandService->getItemsForCollection('winter') as $item) {
        print_r($item);
    }
    print_r($orderedByItemBrandService->getItemsForCollection('winter'));
} catch (\Lpp\Exception\ExceptionInterface $exception) {
    echo 'Sorry. Something was wrong: ' . $exception->getMessage();
}
