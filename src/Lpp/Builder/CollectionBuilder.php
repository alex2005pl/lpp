<?php declare(strict_types=1);

namespace Lpp\Builder;

use Lpp\Entity\Brand;
use Lpp\Entity\Collection;
use Lpp\Entity\Item;
use Lpp\Entity\Price;
use Lpp\Exception\JsonDecodeException;

final class CollectionBuilder
{
    public function fromJson(string $json): Collection
    {
        $data = json_decode($json, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new JsonDecodeException(json_last_error_msg());
        }

        return new Collection($data['id'], $data['collection'], $this->createBrands($data['brands']));
    }

    /** @return Brand[] */
    private function createBrands(array $brandsData): array
    {
        return array_map(function ($brandData) {
            return new Brand($brandData['name'], $brandData['description'], $this->createItems($brandData['items']));
        }, $brandsData);
    }

    /** @return Item[] */
    private function createItems(array $itemsData): array
    {
        return array_map(function ($itemData) {
            return new Item($itemData['name'], $itemData['url'], $this->createPrices($itemData['prices']));
        }, $itemsData);
    }

    /** @return Price[] */
    private function createPrices(array $pricesData): array
    {
        return array_map(function ($priceData) {
            return new Price(
                $priceData['description'],
                $priceData['priceInEuro'],
                new \DateTimeImmutable($priceData['arrival']),
                new \DateTimeImmutable($priceData['due']));
        }, $pricesData);
    }
}
