<?php declare(strict_types=1);

namespace Lpp\Common;

use Lpp\Exception\Resource\ResourceReadingException;
use Lpp\Exception\Resource\ResourceNotFoundException;

final class ResourceLoader implements ResourceLoaderInterface
{
    private $resourceDirectory;

    public function __construct(string $resourceDirectory)
    {
        $this->resourceDirectory = $resourceDirectory;
    }

    public function load(string $filename): string
    {
        $filePath = $this->resourceDirectory . '/' . $filename;
        if (!file_exists($filePath)) {
            throw new ResourceNotFoundException($filePath);
        }

        $resource = file_get_contents($filePath);
        if (false === $resource) {
            throw new ResourceReadingException($filePath);
        }

        return $resource;
    }
}
