<?php declare(strict_types=1);

namespace Lpp\Common;

interface ResourceLoaderInterface
{
    public function load(string $filename): string;
}
