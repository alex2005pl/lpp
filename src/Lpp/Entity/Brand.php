<?php declare(strict_types=1);

namespace Lpp\Entity;

final class Brand
{
    private $name;
    private $description;

    /**
     * Unsorted list of items with their corresponding prices.
     *
     * @var Item[]
     */
    private $items;

    public function __construct(string $name, string $description, array $items)
    {
        $this->name = $name;
        $this->description = $description;
        $this->items = $items;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /** @return Item[] */
    public function getItems(): array
    {
        return $this->items;
    }
}
