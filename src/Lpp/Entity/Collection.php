<?php declare(strict_types=1);

namespace Lpp\Entity;

final class Collection
{
    private $id;
    private $name;
    private $brands;

    public function __construct(int $id, string $name, array $brands)
    {
        $this->id = $id;
        $this->name = $name;
        $this->brands = $brands;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /** @return Brand[] */
    public function getBrands(): array
    {
        return $this->brands;
    }
}
