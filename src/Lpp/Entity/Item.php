<?php declare(strict_types=1);

namespace Lpp\Entity;

use Lpp\Exception\EntityValidationException;
use Lpp\Validator\UrlValidator;

final class Item
{
    private $name;
    private $url;

    /**
     * Unsorted list of prices received from the actual search query.
     *
     * @var Price[]
     */
    private $prices;

    public function __construct(string $name, string $url, array $prices)
    {
        if (!(new UrlValidator())->isValid($url)) {
            throw new EntityValidationException('The URL is invalid.');
        }
        $this->name = $name;
        $this->url = $url;
        $this->prices = $prices;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    /** @return Price[] */
    public function getPrices(): array
    {
        return $this->prices;
    }
}
