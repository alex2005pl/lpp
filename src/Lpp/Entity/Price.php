<?php declare(strict_types=1);

namespace Lpp\Entity;

final class Price
{
    private $description;
    private $priceInEuro;

    /**
     * Warehouse's arrival date (to)
     *
     * @var \DateTimeImmutable
     */
    private $arrivalDate;

    /**
     * Due to date,
     * defining how long will the item be available for sale (i.e. in a collection)
     *
     * @var \DateTimeImmutable
     */
    private $dueDate;

    public function __construct(
        string $description,
        int $priceInEuro,
        \DateTimeImmutable $arrivalDate,
        \DateTimeImmutable $dueDate
    ) {
        $this->description = $description;
        $this->priceInEuro = $priceInEuro;
        $this->arrivalDate = $arrivalDate;
        $this->dueDate = $dueDate;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getPriceInEuro(): int
    {
        return $this->priceInEuro;
    }

    public function getArrivalDate(): \DateTimeImmutable
    {
        return $this->arrivalDate;
    }

    public function getDueDate(): \DateTimeImmutable
    {
        return $this->dueDate;
    }
}
