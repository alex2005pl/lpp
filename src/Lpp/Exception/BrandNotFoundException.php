<?php declare(strict_types=1);

namespace Lpp\Exception;

final class BrandNotFoundException extends \DomainException implements ExceptionInterface
{

}
