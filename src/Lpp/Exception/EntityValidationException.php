<?php declare(strict_types=1);

namespace Lpp\Exception;

final class EntityValidationException extends \DomainException implements ExceptionInterface
{

}
