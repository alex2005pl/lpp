<?php declare(strict_types=1);

namespace Lpp\Exception;

final class JsonDecodeException extends \RuntimeException implements ExceptionInterface
{

}
