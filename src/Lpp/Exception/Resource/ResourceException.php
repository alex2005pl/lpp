<?php declare(strict_types=1);

namespace Lpp\Exception\Resource;

use Lpp\Exception\ExceptionInterface;

class ResourceException extends \RuntimeException implements ExceptionInterface
{
    protected $path;

    public function __construct(string $message = null, string $path = null)
    {
        $this->path = $path;

        if (null === $message) {
            $message = 'Resource load failed.';
            if (null !== $path) {
                $message = sprintf('Resource "%s" load failed.', $path);
            }
        }

        parent::__construct($message);
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
