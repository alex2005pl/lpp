<?php declare(strict_types=1);

namespace Lpp\Exception\Resource;

final class ResourceNotFoundException extends ResourceException
{
    public function __construct(string $path = null)
    {
        $message = 'Resource could not be found.';
        if (null !== $path) {
            $message = sprintf('Resource "%s" could not be found.', $path);
        }

        parent::__construct($message, $path);
    }
}
