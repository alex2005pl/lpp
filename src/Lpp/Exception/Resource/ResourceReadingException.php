<?php declare(strict_types=1);

namespace Lpp\Exception\Resource;

final class ResourceReadingException extends ResourceException
{
    public function __construct(string $path = null)
    {
        $message = 'Resource could not be read.';
        if (null !== $path) {
            $message = sprintf('Resource "%s" could not be read.', $path);
        }

        parent::__construct($message, $path);
    }
}
