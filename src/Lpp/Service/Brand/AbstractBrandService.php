<?php declare(strict_types=1);

namespace Lpp\Service\Brand;

use Lpp\Entity\Brand;
use Lpp\Exception\BrandNotFoundException;
use Lpp\Service\Item\ItemServiceInterface;

abstract class AbstractBrandService implements BrandServiceInterface
{
    protected $itemService;
    protected $collectionNameToIdMapping = [
        'winter' => 1315475,
    ];

    public function __construct(ItemServiceInterface $itemService)
    {
        $this->itemService = $itemService;
    }

    /** @return Brand[] */
    public function getBrandsForCollection(string $collectionName): array
    {
        if (!key_exists($collectionName, $this->collectionNameToIdMapping)) {
            throw new BrandNotFoundException("Provided collection name [{$collectionName}] is not mapped.");
        }

        $collectionId = $this->collectionNameToIdMapping[$collectionName];

        return $this->itemService->getResultForCollectionId($collectionId);
    }

    public function getItemsForCollection(string $collectionName): array
    {
        $items = [];
        foreach ($this->getBrandsForCollection($collectionName) as $brand) {
            $items = array_merge($items, $brand->getItems());
        }

        return $items;
    }

    public function setItemService(ItemServiceInterface $itemService): void
    {
        $this->itemService = $itemService;
    }
}
