<?php declare(strict_types=1);

namespace Lpp\Service\Brand;

use Lpp\Entity\Item;

final class OrderedByItemNameBrandService extends AbstractBrandService
{
    public function getItemsForCollection(string $collectionName): array
    {
        return $this->sortItemsByName(parent::getItemsForCollection($collectionName));
    }

    private function sortItemsByName(array $items): array
    {
        usort($items, function (Item $item1, Item $item2) {
            return strcasecmp($item1->getName(), $item2->getName());
        });

        return $items;
    }
}
