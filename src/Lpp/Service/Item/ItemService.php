<?php declare(strict_types=1);

namespace Lpp\Service\Item;

use Lpp\Builder\CollectionBuilder;
use Lpp\Common\ResourceLoader;

final class ItemService implements ItemServiceInterface
{
    private $resourceLoader;
    private $builder;

    public function __construct(ResourceLoader $resourceLoader, CollectionBuilder $builder)
    {
        $this->resourceLoader = $resourceLoader;
        $this->builder = $builder;
    }

    public function getResultForCollectionId(int $collectionId): array
    {
        return $this->builder->fromJson($this->resourceLoader->load($collectionId . '.json'))->getBrands();
    }
}
