<?php declare(strict_types=1);

namespace Lpp\Validator;

final class UrlValidator implements ValidatorInterface
{
    public function isValid($value): bool
    {
        return (false !== filter_var($value, FILTER_VALIDATE_URL));
    }
}
