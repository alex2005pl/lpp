<?php declare(strict_types=1);

namespace Lpp\Validator;

interface ValidatorInterface
{
    public function isValid($value): bool;
}
