<?php declare(strict_types=1);

namespace Lpp\Tests\Common;

use Lpp\Common\ResourceLoader;
use Lpp\Exception\Resource\ResourceNotFoundException;
use PHPUnit\Framework\TestCase;

final class ResourceLoaderTest extends TestCase
{
    private const RESOURCE_DIR = __DIR__ . '/../../data';

    public function testCanLoadJsonResource(): void
    {
        $loader = new ResourceLoader(self::RESOURCE_DIR);

        $this->assertIsString($loader->load('1315475.json'));
    }

    public function testItThrowExceptionWhenResourceNotExists(): void
    {
        $loader = new ResourceLoader(self::RESOURCE_DIR);

        $this->expectException(ResourceNotFoundException::class);

        $loader->load('abcd.json');
    }
}
