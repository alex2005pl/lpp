<?php declare(strict_types=1);

namespace Lpp\Tests\Entity;

use Lpp\Entity\Item;
use Lpp\Exception\EntityValidationException;
use PHPUnit\Framework\TestCase;

final class ItemTest extends TestCase
{
    public function testCanBeCreatedWithValidUrl(): void
    {
        $this->assertInstanceOf(
            Item::class,
            new Item('hat', 'https://www.lppsa.com/', [])
        );
    }

    public function testCannotBeCreatedWithInvalidUrl(): void
    {
        $this->expectException(EntityValidationException::class);

        new Item('hat', 'www.www', []);
    }
}
