<?php declare(strict_types=1);

namespace Lpp\Tests\Validator;

use Lpp\Validator\UrlValidator;
use PHPUnit\Framework\TestCase;

final class UrlValidatorTest extends TestCase
{
    public function testIsValid(): void
    {
        $validator = new UrlValidator();

        $this->assertTrue($validator->isValid('https://www.lppsa.com/'));
    }

    public function testIsNotValid(): void
    {
        $validator = new UrlValidator();

        $this->assertFalse($validator->isValid('https//www.lppsa.com/'));
    }
}
